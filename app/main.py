#System imports

#Libs imports
from fastapi import FastAPI

from routers import admins
from routers import entreprises
from routers import notifications
#Local imports
from routers import users
from internal import auth

app = FastAPI()

app.include_router(auth.router, tags=["Authentification"])
app.include_router(users.router, tags=["User"])
app.include_router(entreprises.router, tags=["Entreprise"])
app.include_router(admins.router, tags=["Plannings"])
app.include_router(notifications.router, tags=["Notifications"])