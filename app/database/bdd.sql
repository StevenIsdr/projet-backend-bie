CREATE TABLE entreprises (
  id INTEGER PRIMARY KEY,
  nom TEXT
);

CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  nom TEXT,
  prenom TEXT,
  email TEXT,
  password TEXT,
  entreprise_id INTEGER,
  role TEXT,
  FOREIGN KEY (entreprise_id) REFERENCES Entreprises(id)
);

CREATE TABLE plannings (
  id INTEGER PRIMARY KEY,
  nom TEXT,
  entreprise_id INTEGER,
  date_debut DATE,
  date_fin DATE,
  FOREIGN KEY (entreprise_id) REFERENCES Entreprises(id)
);

CREATE TABLE activites (
  id INTEGER PRIMARY KEY,
  nom TEXT,
  planning_id INTEGER,
  date_debut DATE,
  date_fin DATE,
  type TEXT,
  FOREIGN KEY (planning_id) REFERENCES plannings(id)
);

CREATE TABLE users_activites (
  user_id INTEGER,
  activite_id INTEGER,
  utilisateur_activite_date_debut DATE,
  utilisateur_activite_date_fin DATE,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (activite_id) REFERENCES activites(id)
);
