# System imports
import hashlib
from datetime import datetime
from typing import Annotated

# Libs imports
from fastapi import APIRouter, Depends, status, Response, HTTPException
import sqlite3

# Local imports
from internal.models import User
from internal.models import Activite
from internal.crypto import encrypt_data, decrypt_data
from routers.notifications import create_notification
from internal.models import Entreprise

from internal.auth import decode_token

router = APIRouter()

conn = sqlite3.connect('database/bdd')
cursor = conn.cursor()


@router.get("/activites")
async def get_activites(user: Annotated[User, Depends(decode_token)]):
    # Get entreprise id from user, then get all activites from entreprise, if user is maintainer or admin, show all activites if not show only user activites
    if user.role == "Maintainer":
        cursor.execute("SELECT * FROM activites")
        activites = cursor.fetchall()
        return activites
    elif user.role == "Admin":
        # Get all activites from entreprise, to do so, get the id of all users in the entreprise, then get all activites thoose users
        cursor.execute("SELECT * FROM users WHERE entreprise_id=?", (user.entreprise_id,))
        users = cursor.fetchall()
        for userObj in users:
            cursor.execute("SELECT * FROM activites WHERE created_by=?", (userObj[0],))
            activites = cursor.fetchall()
        return activites
    else:
        cursor.execute("SELECT * FROM activites where created_by=?", (user.id,))
        activites = cursor.fetchall()
        return activites


@router.get("/activites/{activite_id}")
async def get_activite(user: Annotated[User, Depends(decode_token)], activite_id: int):
    # Get entreprise id from user, then get all activites from entreprise, if user is maintainer or admin, show all activites if not show only user activites
    if user.role == "Maintainer":
        cursor.execute("SELECT * FROM activites WHERE id=?", (activite_id,))
        activite = cursor.fetchone()
        return activite
    elif user.role == "Admin":
        # Get all activites from entreprise, to do so, get the id of all users in the entreprise, then get all activites thoose users
        cursor.execute("SELECT * FROM users WHERE entreprise_id=?", (user.entreprise_id,))
        users = cursor.fetchall()
        for userObj in users:
            cursor.execute("SELECT * FROM activites WHERE created_by=?", (userObj[0],))
            activites = cursor.fetchall()
        return activites
    else:
        cursor.execute("SELECT * FROM activites WHERE id=? AND created_by=?", (activite_id, user.id))
        activite = cursor.fetchone()
        return activite


@router.post("/activites", status_code=status.HTTP_201_CREATED)
async def create_activite(user: Annotated[User, Depends(decode_token)], new_activite: Activite) -> Activite:
    date_debut_obj = datetime.strptime(new_activite.date_debut, '%d-%m-%Y').date()
    date_fin_obj = datetime.strptime(new_activite.date_fin, '%d-%m-%Y').date()
    # Check if planning_id is in entreprise_id
    cursor.execute("SELECT * FROM plannings WHERE id=?", (new_activite.planning_id,))
    planning = cursor.fetchone()
    if planning is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Planning not found")
    if planning[2] != user.entreprise_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to create activite")
    cursor.execute(
        "INSERT INTO activites (nom, planning_id,date_debut,date_fin,type, created_by) VALUES (?, ?, ?, ?, ? ,? )", (
            new_activite.nom, new_activite.planning_id, date_debut_obj, date_fin_obj, new_activite.type, user.id))
    conn.commit()
    return new_activite


# Join acitivity if user in in the same entreprise as the activity
@router.post("/activites/{id}")
async def join_activite(user: Annotated[User, Depends(decode_token)], id: int):
    cursor.execute("SELECT * FROM activites WHERE id=?", (id,))
    activite = cursor.fetchone()
    if activite is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Activity not found")
    cursor.execute("SELECT * FROM users WHERE id=?", (user.id,))
    userObj = cursor.fetchone()
    # From the activity, get the planning and get the entreprise from the planning
    cursor.execute("SELECT * FROM plannings WHERE id=?", (activite[2],))
    planning = cursor.fetchone()
    if planning[2] != userObj[5]:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail="You don't have the right to join this activity")
    cursor.execute("INSERT INTO users_activites (user_id,activite_id) VALUES (?, ?)", (user.id, id))
    conn.commit()
    return Response(status_code=status.HTTP_200_OK)


# Leave activity if user is in activity
@router.delete("/activites/{id}")
async def leave_activite(user: Annotated[User, Depends(decode_token)], id: int):
    cursor.execute("SELECT * FROM activites WHERE id=?", (id,))
    activite = cursor.fetchone()
    if activite is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Activity not found")
    cursor.execute("SELECT * FROM users_activites WHERE user_id=? AND activite_id=?", (user.id, id))
    user_activite = cursor.fetchone()
    if user_activite is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not in activity")
    cursor.execute("DELETE FROM users_activites WHERE user_id=? AND activite_id=?", (user.id, id))
    conn.commit()
    create_notification(user.id, "Vous avez quitté l'activité")
    return Response(status_code=status.HTTP_200_OK)


@router.post("/users", status_code=status.HTTP_201_CREATED)
async def create_user(user: Annotated[User, Depends(decode_token)], new_user: User) -> User:
    if user.role == "Maintainer":
        new_user.password = hashlib.sha256(new_user.password.encode()).hexdigest()
        cursor.execute(
            "INSERT INTO users (nom, prenom, email, password, entreprise_id, role) VALUES (?, ?, ?, ?, ?, ?)", (
                encrypt_data(new_user.nom), encrypt_data(new_user.prenom), new_user.email, new_user.password,
                new_user.entreprise_id, new_user.role))
        conn.commit()
        return new_user
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to create a user")


@router.delete("/users/{user_id}")
async def delete_user(user: Annotated[User, Depends(decode_token)], user_id: int) -> User:
    if user.role == "Maintainer":
        cursor.execute("DELETE FROM users WHERE id=?", (user_id,))
        conn.commit()
        return User(id=user_id, nom="deleted", prenom="deleted", email="deleted", password="deleted", entreprise_id=0,
                    role="deleted")
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to delete a user")


@router.get("/users")
async def get_all_users(user: Annotated[User, Depends(decode_token)]):
    if user.role == "Maintainer":
        cursor.execute("SELECT * FROM users")
        users = cursor.fetchall()
        return [
            User(id=user[0], nom=decrypt_data(user[1]), prenom=decrypt_data(user[2]), email=user[3], password=user[4],
                 entreprise_id=user[5], role=user[6]) for user in users]
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to see users")


@router.get("/users/{user_id}")
async def get_user(user: Annotated[User, Depends(decode_token)], user_id: int):
    if user.role == "Maintainer":
        cursor.execute("SELECT * FROM users WHERE id=?", (user_id,))
        user = cursor.fetchone()
        return User(id=user[0], nom=decrypt_data(user[1]), prenom=decrypt_data(user[2]), email=user[3],
                    password=user[4], entreprise_id=user[5], role=user[6])
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to see user")


@router.put("/users/{user_id}")
async def update_user(user: Annotated[User, Depends(decode_token)], user_id: int, new_user: User) -> User:
    if user.role == "Maintainer":
        new_user.password = hashlib.sha256(new_user.password.encode()).hexdigest()
        cursor.execute("UPDATE users SET nom=?, prenom=?, email=?, password=?, entreprise_id=?, role=? WHERE id=?", (
            encrypt_data(new_user.nom), encrypt_data(new_user.prenom), new_user.email, new_user.password,
            new_user.entreprise_id, new_user.role, user_id))
        conn.commit()
        create_notification(user_id, "Votre compte a été modifié")
        return new_user
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to update user")


@router.post("/activites/{activites_id}/user/{user_id}")
async def set_user_to_planning(
        user: Annotated[User, Depends(decode_token)], activites_id: int,
        user_id: int) -> Entreprise:
    if user.role == "Maintainer" or user.role == "Admin":
        cursor.execute("INSERT INTO users_activites (activites_id,user_id) VALUES (?, ?)",
                       (activites_id, user_id))
        conn.commit()
        return Entreprise(id=cursor.lastrowid, nom="updated")
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail="You don't have the right to update planning")

@router.delete("/activites/{activite_id}/user/{user_id}")
async def delete_user_to_planning(
        user: Annotated[User, Depends(decode_token)], activite_id: int,
        user_id: int) -> Entreprise:
    if user.role == "Maintainer" or user.role == "Admin":
        cursor.execute("DELETE FROM users_activites WHERE activite_id=? AND user_id=?",
                       (activite_id, user_id))
        conn.commit()
        return Entreprise(id=cursor.lastrowid, nom="updated")
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail="You don't have the right to update planning")