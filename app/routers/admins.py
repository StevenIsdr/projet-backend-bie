# System imports
import hashlib
from datetime import datetime
from typing import Annotated

# Libs imports
from fastapi import APIRouter, Depends, status, Response, HTTPException
import sqlite3

# Local imports
from internal.models import User, Entreprise
from internal.auth import decode_token
from routers.notifications import create_notification
from internal.models import Planning

router = APIRouter()

conn = sqlite3.connect('database/bdd')
cursor = conn.cursor()


@router.get("/planning")
async def get_entreprise_planning(user: Annotated[User, Depends(decode_token)]):
    if user.role == "Maintainer" or user.role == "Admin":
        cursor.execute("SELECT * FROM plannings WHERE entreprise_id=?", (user.entreprise_id,))
        planning = cursor.fetchall()
        return planning
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to see planning")


@router.post("/planning", status_code=status.HTTP_201_CREATED)
async def create_entreprise_planning(user: Annotated[User, Depends(decode_token)], nom: str, date_debut: str,
                                     date_fin: str) -> Entreprise:
    date_debut_obj = datetime.strptime(date_debut, '%d-%m-%Y').date()
    date_fin_obj = datetime.strptime(date_fin, '%d-%m-%Y').date()
    if user.role == "Maintainer" or user.role == "Admin":
        cursor.execute("INSERT INTO plannings (nom,entreprise_id,date_debut,date_fin) VALUES (?, ?, ?, ?)",
                       (nom, user.entreprise_id, date_debut_obj, date_fin_obj))
        conn.commit()
        create_notification(user.id, "Vous avez quitté l'activité")
        return Planning(id=cursor.lastrowid, nom=nom, entreprise_id=user.entreprise_id, date_debut=date_debut,
                        date_fin=date_fin)
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to create planning")


@router.delete("/planning/{planning_id}")
async def delete_entreprise_planning(user: Annotated[User, Depends(decode_token)], planning_id: int) -> Entreprise:
    if user.role == "Maintainer" or user.role == "Admin":
        # before deleting the planning, delete all the activities and users_activities associated with it
        # Users_activities only has activity_id, so we need to get the activities associated with the planning
        cursor.execute("SELECT * FROM activites WHERE planning_id=?", (planning_id,))
        activities = cursor.fetchall()
        for activity in activities:
            cursor.execute("DELETE FROM users_activites WHERE activity_id=?", (activity[0],))
            conn.commit()
        cursor.execute("DELETE FROM activites WHERE planning_id=?", (planning_id,))
        conn.commit()
        cursor.execute("DELETE FROM plannings WHERE id=?", (planning_id,))
        conn.commit()
        return Entreprise(id=planning_id, nom="deleted")
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to delete planning")


@router.put("/planning")
async def update_entreprise_planning(user: Annotated[User, Depends(decode_token)], date_debut: str,
                                     date_fin: str) -> Entreprise:
    date_debut_obj = datetime.strptime(date_debut, '%d-%m-%Y').date()
    date_fin_obj = datetime.strptime(date_fin, '%d-%m-%Y').date()
    if user.role == "Maintainer" or user.role == "Admin":
        cursor.execute("UPDATE planning SET date_debut=?, date_fin=? WHERE entreprise_id=?",
                       (date_debut_obj, date_fin_obj, user.entreprise_id))
        conn.commit()
        return Planning(id=cursor.lastrowid, nom="updated", entreprise_id=user.entreprise_id, date_debut=date_debut_obj,
                        date_fin=date_fin_obj)
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to update planning")
