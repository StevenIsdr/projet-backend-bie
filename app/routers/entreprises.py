#System imports
import hashlib
from typing import Annotated

#Libs imports
from fastapi import APIRouter,Depends, status, Response, HTTPException
import sqlite3

#Local imports
from internal.models import User, Entreprise
from internal.auth import decode_token
from internal.crypto import decrypt_data

router = APIRouter()

conn = sqlite3.connect('database/bdd')
cursor = conn.cursor()

@router.get("/entreprise")
async def get_all_entreprise(user: Annotated[User, Depends(decode_token)]):
    if user.role == "Maintainer":
        cursor.execute("SELECT * FROM entreprises")
        entreprises = cursor.fetchall()
        return [Entreprise(id=entreprise[0], nom=entreprise[1]) for entreprise in entreprises]
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to see entreprises")

@router.post("/entreprise", status_code=status.HTTP_201_CREATED)
async def create_entreprise(user: Annotated[User, Depends(decode_token)],nom: str) -> Entreprise:
    if user.role == "Maintainer":
        cursor.execute("INSERT INTO entreprises (nom) VALUES (?)", (nom,))
        conn.commit()
        return Entreprise(id=cursor.lastrowid, nom=nom)
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to create entreprise")

#get entreprise if maintainer or admin, else get only if it's the user's entreprise
@router.get("/entreprise/{entreprise_id}")
async def get_entreprise(user: Annotated[User, Depends(decode_token)],entreprise_id: int) -> Entreprise:
    if user.role == "Maintainer":
        cursor.execute("SELECT * FROM entreprises WHERE id=?", (entreprise_id,))
        entreprise = cursor.fetchone()
        return Entreprise(id=entreprise[0], nom=entreprise[1])
    elif user.entreprise_id == entreprise_id:
        cursor.execute("SELECT * FROM entreprises WHERE id=?", (entreprise_id,))
        entreprise = cursor.fetchone()
        return Entreprise(id=entreprise[0], nom=entreprise[1])
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to see entreprise")

@router.put("/entreprise/{entreprise_id}")
async def update_entreprise(user: Annotated[User, Depends(decode_token)],entreprise_id: int, nom: str) -> Entreprise:
    if user.role == "Maintainer":
        cursor.execute("UPDATE entreprises SET nom=? WHERE id=?", (nom, entreprise_id))
        conn.commit()
        return Entreprise(id=entreprise_id, nom=nom)
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to update entreprise")
@router.delete("/entreprise/{entreprise_id}")
async def delete_entreprise(user: Annotated[User, Depends(decode_token)],entreprise_id: int) -> Entreprise:
    if user.role == "Maintainer":
        cursor.execute("DELETE FROM entreprises WHERE id=?", (entreprise_id,))
        conn.commit()
        return Entreprise(id=entreprise_id, nom="deleted")
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to delete entreprise")

#get all users from an entreprise
@router.get("/entreprise/{entreprise_id}/users")
async def get_entreprise_users(user: Annotated[User, Depends(decode_token)],entreprise_id: int):
    if user.role == "Maintainer" or user.role == "Admin":
        if user.role == "Admin" and user.entreprise_id != entreprise_id:
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to see users from another entreprise")
        cursor.execute("SELECT * FROM users WHERE entreprise_id=?", (entreprise_id,))
        users = cursor.fetchall()
        return [User(id=user[0], nom=decrypt_data(user[1]), prenom=decrypt_data(user[2]), email=user[3],password=user[4], entreprise_id=user[5], role=user[6]) for user in users]
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to see users")