#System imports
import hashlib
from typing import Annotated

#Libs imports
from fastapi import APIRouter,Depends, status, Response, HTTPException
import sqlite3

#Local imports
from internal.models import User, Entreprise
from internal.auth import decode_token

router = APIRouter()

conn = sqlite3.connect('database/bdd',check_same_thread=False)
cursor = conn.cursor()
#Get user notification
@router.get("/notifications")
def get_user_unread_notifications(user: Annotated[User, Depends(decode_token)]):
    cursor.execute("SELECT * FROM notifications WHERE user_id=? AND read_at IS NULL", (user.id,))
    notifications = cursor.fetchall()
    cursor.execute("UPDATE notifications SET read_at=datetime('now') WHERE user_id=?", (user.id,))
    conn.commit()
    return notifications

@router.get("/notifications/read")
def get_user_read_notifications(user: Annotated[User, Depends(decode_token)]):
    cursor.execute("SELECT * FROM notifications WHERE user_id=? AND read_at IS NOT NULL", (user.id,))
    notifications = cursor.fetchall()
    return notifications

def create_notification(user_id: int, message: str):
    cursor.execute("INSERT INTO notifications (user_id, notification) VALUES (?,?)", (user_id, message))
    conn.commit()
    return cursor.lastrowid

#Delete notif if user
@router.delete("/notifications/{notif_id}")
def delete_user_notification(user: Annotated[User, Depends(decode_token)], notif_id: int):
    cursor.execute("SELECT * FROM notifications WHERE id=?", (notif_id,))
    notification = cursor.fetchone()
    if notification[1] == user.id:
        cursor.execute("DELETE FROM notifications WHERE id=?", (notif_id,))
        conn.commit()
        return notification
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have the right to delete this notification")
