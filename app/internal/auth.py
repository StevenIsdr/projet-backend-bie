#System imports

from typing import Annotated
import hashlib

#Libs imports
from fastapi import Depends, APIRouter, status, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from pydantic import BaseModel
import sqlite3


#Local imports
from internal.crypto import encrypt_data, decrypt_data

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

class User(BaseModel):
    id: int
    username: str
    role: str
    entreprise_id: int


JWT_KEY = "kajshkdalasjjlhgkjguifoudhsfkxahdsf"

conn = sqlite3.connect('database/bdd')
cursor = conn.cursor()

async def decode_token(token: Annotated[str, Depends(oauth2_scheme)]) -> User:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        decoded_data = jwt.decode(token, JWT_KEY, algorithms=['HS256'])
        #TODO: verify that the user actually exists, for example if it was deleted since the JWT was emited
        return User(role = decoded_data["role"], username = decoded_data["username"], id = decoded_data["id"], entreprise_id = decoded_data["entreprise_id"])
    except JWTError:
        return credentials_exception


@router.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    # Interrogation de la base de données pour trouver l'utilisateur
    hash = hashlib.sha256(form_data.password.encode()).hexdigest()
    cursor.execute("SELECT * FROM users WHERE email=? AND password=?", (form_data.username, hash))
    user = cursor.fetchone()

    if user is None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect username or password")

    data = {"id": user[0],"username": decrypt_data(user[1]),"role": user[6],"entreprise_id": user[5]}
    jwt_token = jwt.encode(data, JWT_KEY, algorithm="HS256")
    return {"access_token": jwt_token, "token_type": "bearer"}


    