from typing import Optional
from pydantic import BaseModel

class Entreprise(BaseModel):
    id: int
    nom: str

class User(BaseModel):
    id: int
    nom: str
    prenom: Optional[str]
    email: str
    password: str
    entreprise_id: int
    role: str

class Planning(BaseModel):
    id: int
    nom: str
    entreprise_id: int
    date_debut: str
    date_fin: str

class Activite(BaseModel):
    id: int
    nom: str
    planning_id: int
    date_debut: str
    date_fin: str
    type: str

class UserActivite(BaseModel):
    user_id: int
    activite_id: int
    utilisateur_activite_date_debut: str
    utilisateur_activite_date_fin: str
