# Projet BIE - Backend de gestion d'activités et de plannings FAST API

Ce projet est le backend de gestion d'activités, de plannings et d'utilisateurs. Il permet de créer, modifier et consulter des activités, de planifier ces activités dans des plannings, et de gérer les utilisateurs du système.

## Fonctionnalités

L'application offre les fonctionnalités suivantes :

- Création, modification et suppression d'activités.
- Création, modification et suppression de plannings.
- Association d'activités à des plannings.
- Gestion des utilisateurs (création, modification, suppression).
- Authentification et autorisation basées sur les rôles des utilisateurs (User,Admin,Maintainer).
- Consultation des activités et plannings disponibles.
- Autres...

## Prérequis

Avant de commencer, assurez-vous d'avoir les éléments suivants :

- Docker installé sur votre machine.

## Installation et exécution

1. Clonez ce dépôt dans le répertoire de votre choix :

```
git clone https://gitlab.com/StevenIsdr/projet-backend-bie.git
```

2. Accédez au répertoire du projet :

```
cd projet-backend-bie
```

3. Lancez l'application à l'aide de Docker Compose :

```
docker-compose up --build
```

Cela va construire les conteneurs Docker nécessaires et démarrer l'application.

4. Accédez à l'application dans votre navigateur :

```
http://0.0.0.0:8000/docs
```

## Configuration

L'application est configurée pour utiliser une base de données SQLite incluse avec le projet. Aucune configuration supplémentaire n'est nécessaire pour démarrer l'application en utilisant Docker Compose.

## Utilisateurs

Voici le tableau d'exemple représentant les utilisateurs du système qui sont actuellement dans le SQLite :

| ID | Nom    | Prénom  | Email                       | Mot de passe | Entreprise ID | Rôle           |
|----|--------|---------|-----------------------------|--------------|---------------|----------------|
| 1  | Admin  | Admin   | admin@projetback.io         | azerty       | 0             | Administrateur |
| 2  | Steven | Isidor  | stevenisidor2002@gmail.com  | azerty       | 1             | Admin          |
| 3  | Steven | Employé | steven@employe.fr | azerty       | 1             | Utilisateur    |

